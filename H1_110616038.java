/**\mainpage 辨認是否為質數程式 Homework 1
 *  - \subpage 名稱：H1_110616038
 *  - \subpage 作者：110616038 陳泰元
 *  - \subpage 版本：1.0
 *  - \subpage 日期：2018/10/27
 *  
 *  操作說明：
 *  這個程式將可以為您判別該數是否為質數
 *  程式會提示您輸入符合標準的數字
 *  (如果輸入錯誤程式會提醒您)
 *  如果結果並非質數時
 *  則會輸出質因數分解的結果在螢幕上
 *  完畢後程式會問您是否需要再次服務
 *  請依照您的意願選擇～
 *  
 *  獨特功能：
 *  - \subpage 利用函式使程式碼簡潔明白
 *  - \subpage 使用遞迴(Recursive)函式
 *  - \subpage 當結果為非質數時 會輸出質因數分解
 *  - \subpage 利用Doxygen編寫說明文件(110616038資料夾中)
 *  - \subpage 當使用者輸入非正整數時會提醒使用者
 *  - \subpage 會詢問使用者是否繼續使用服務(輸入錯誤時也會提醒)
 *  
 *  符合的評分標準：
 *  - \subpage 程式有意義且可以執行 (+10%) 
 *  - \subpage 正確顯示答案 (+70%)
 *  - \subpage 顯示其因式分解可供驗證是否為質數 (+20%) 
 *  
 *  自評分數：
 *  99分（人總是無法盡善盡美 但是可以全力以赴！）
 */

import java.util.Scanner;

public class H1_110616038 {

	private static Scanner cin;

	/**
	 * @brief 此處為程式開始點
	 * @param [in] String[] args
	 */
	public static void main(String[] args) {
		
		
		int tempint; /** int tempint< 用於儲存使用者輸入的數字 */
		cin = new Scanner(System.in);
		
		while(true) {
			System.out.println("請輸入一個大於0的正整數!"); //請使用者輸入數字
			tempint = cin.nextInt();
			
			//此處將辨別使用者輸入的數字是否能夠繼續執行步驟
			if(isCorrect(tempint)) {
				//執行判斷質數步驟
				getPrime(tempint);
				//呼叫isGoing函式詢問使用者是否繼續使用
				if(!isGoing()) {
					break;
				}
			}
			else {
				//請求再輸入一次
				System.out.println("您輸入錯誤！請檢查該數是否為大於0的正整數");
			}
		}
	}
	
	
	/**
	 * @brief isCorrect用於判別使用者輸入的值是否符合標準 
	 * - \subpage 標準：必須大於0的正整數 
	 * 
	 * @param [in] tempInt 暫存使用者輸入的變數
	*/
	public static boolean  isCorrect(int tempInt) {
		
		if(tempInt>0) {
			return true;  /**@return 合乎標準則回傳true */
		}
		else {
			return false; /**@return 反之則回傳false */			 
		}
	}
	
	/**
	 * @brief getPrime用於判別使用者輸入的值是否為質數
	 * - \subpage 如果為1->則會輸出“此數為1 定義上不屬於質數”
	 * - \subpage 如果為質數->則會輸出“此數屬於質數”
	 * - \subpage 如果不質數->則會輸出其因數分解 以及 “此數不屬於質數”
	 * 
	 * @param [in] tempInt 欲判斷是否為質數的數字
	*/
	public static void getPrime(int tempInt) {
		
		boolean check = true; /** boolean check< 用來做該數是否為質數的標記 */
		
		if(tempInt==1) {
			System.out.println("此數為1 定義上不屬於質數");
			check = false;
			return;
		}
		else {
			for(int i=2;i<tempInt;i++) { //從比自己小的數字開始除
				//如果能整除代表除了自己和1外 還有其他因數
				if(tempInt%i == 0) {
					System.out.println("質數因數分解為");
					intFtz(tempInt); //呼叫因數分解函式
					System.out.println("驗證此數不屬於質數");
					check = false;
					return; //可以不用繼續往下找
				}
			}	
		}
		
		if(check==true) {
			System.out.println("此數屬於質數"); //質數的最後確認
		}
	}
	
	/**
	 * @brief intFtz用於做質因數分解
	 * - \subpage 其中有使用 遞迴(Recursive)
	 * 
	 * @param [in] tempInt 為欲做質因數分解的數
	*/
	public static void intFtz(int tempInt) {
		
		for(int i=2;i<=tempInt;i++) { //每一次皆從比自己小的數字開始除 直到遇到自己
			
			/*當找到自己本身的時候
			 * 就代表已經找完了
			 * 不需要再呼叫一次自己
			 */
			if(tempInt==i) {
				System.out.println(i);
				return;
			}
			
			if(tempInt%i == 0) {
				System.out.print(i+"*");
				intFtz(tempInt/i);	//將 商 再次做質因數分解
				return;  //等所有數字皆分解完後 再度回到呼叫它的函式
			}
		}
	}
	
	/**
	 * @brief isGoing用於詢問使用者繼續使用服務的意願
	 * - \subpage 其中如果輸入非0或非1的數字會要求再選擇一次
	 * 
	 * @return true or false  需要->true 不需要->false 
	*/
	public static boolean isGoing() {
		
		int choose;//記錄使用者的選擇
		
		while(true) {//等待使用者作出正確的選擇
			System.out.println("請問您還需要使用本服務嗎？(1:需要/0:不需要)"); 
			choose = cin.nextInt();
			if(choose!=1 && choose!=0) {
				System.out.println("您輸入錯誤！請再選擇一次！");
			}
			else if(choose==1) {
				System.out.println("好的！請稍等！");
				return true;
			}
			else {
				System.out.println("好的！感謝您的使用！");
				return false;
			}
		}
	}
}
